import os
from os import abort

from flask import render_template, url_for, current_app, request
from flask_login import login_required, current_user
from werkzeug.utils import redirect, secure_filename

from . import admin_bp
from .forms import PostForm, UserAdminForm
from app.models import Post
from ..auth.decorators import admin_required
from ..auth.models import User


@admin_bp.route("/admin/posts")
@login_required
@admin_required
def list_posts():
    #posts = Post.get_all()
    page = int(request.args.get('page', 1))
    per_page = current_app.config['ITEMS_PER_PAGE']
    post_pagination = Post.all_paginated(page, per_page)
    return render_template('admin/posts.html', post_pagination=post_pagination)


@admin_bp.route("/admin/post/", methods=['GET', 'POST'])
@login_required
@admin_required
def post_form():
    form = PostForm()
    if form.validate_on_submit():
        title = form.title.data
        content = form.content.data
        file = form.post_image.data
        image_name = None
        if file:
            image_name = secure_filename(file.filename)
            images_dir = current_app.config['POST_IMAGES_DIR']
            os.makedirs(images_dir, exist_ok=True)
            file_path = os.path.join(images_dir, image_name)
            file.save(file_path)
        post = Post(user_id=current_user.id, title=title, content=content)
        post.image_name = image_name
        post.save()
        current_app.logger.info(f'Guardando nuevo post: {post.title}')
        return redirect(url_for('admin.list_posts'))
    return render_template('admin/post_form.html', form=form)


@admin_bp.route('/admin/post/<int:post_id>/', methods=['GET', 'POST'])
@login_required
@admin_required
def update_post_form(post_id):
    post = Post.get_by_id(post_id)
    if post is None:
        current_app.logger.info(f'El post {post_id} no existe')
        abort(404)
    form = PostForm(obj=post)
    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.content.data
        post.title_slug = form.title_slug.data
        file = form.post_image.data
        image_name = None
        if file:
            image_name = secure_filename(file.filename)
            images_dir = current_app.config['POSTS_IMAGES_DIR']
            os.makedirs(images_dir, exist_ok=True)
            file_path = os.path.join(images_dir, image_name)
            file.save(file_path)
        post.image_name = image_name
        post.save()
        current_app.logger.info(f'El post {post_id} ha sido guardado')
        return redirect(url_for('admin.list_posts'))
    return render_template('admin/post_form.html', form=form, post=post)


@admin_bp.route('/admin/post/delete/<int:post_id>', methods=['POST', ])
@login_required
@admin_required
def delete_post(post_id):
    post = Post.get_by_id(post_id)
    if post is None:
        current_app.logger.info(f'El post a eliminar {post_id} no existe')
        abort(404)
    post.delete()
    current_app.logger.info(f'Se ha eliminado el post {post_id}')
    return redirect(url_for('admin.list_posts'))


@admin_bp.route('/admin/users')
@login_required
@admin_required
def list_users():
    #users = User.get_all()
    page = int(request.args.get('page', 1))
    per_page = current_app.config['ITEMS_PER_PAGE']
    user_pagination = User.all_paginated(page, per_page)
    return render_template('admin/users.html', user_pagination=user_pagination)


@admin_bp.route('/admin/user/<int:user_id>', methods=['GET', 'POST'])
@login_required
@admin_required
def update_user_form(user_id):
    user = User.get_by_id(user_id)
    if user is None:
        current_app.logger.info(f'El usuario {user_id} no existe')
        abort(404)
    form = UserAdminForm(obj=user)
    if form.validate_on_submit():
        user.is_admin = form.is_admin.data
        user.save()
        current_app.logger.info(f'El usuario {user_id} ha sido actualizado')
        return redirect(url_for('admin.list_users'))
    return render_template('admin/user_form.html', form=form, user=user)


@admin_bp.route('/admin/user/delete/<int:user_id>', methods=['GET', 'POST'])
@login_required
@admin_required
def delete_user(user_id):
    user = User.get_by_id(user_id)
    if user is None:
        current_app.logger.info(f'El usuario {user_id} no existe')
        abort(404)
    user.delete()
    current_app.logger.info(f'El usuario {user_id} se ha eliminado')
    return redirect(url_for('admin.list_users'))


@admin_bp.route('/admin/')
@login_required
@admin_required
def index():
    return render_template('admin/index.html')
