from flask import url_for, render_template, request, redirect, current_app
from flask_login import current_user, login_user, logout_user
from werkzeug.urls import url_parse

from app import login_manager
from . import auth_bp
from .forms import SignupForm, LoginForm
from .models import User
from app.common.mail import send_email

@auth_bp.route('/signup/', methods=['GET', 'POST'])
def show_signup_form():
    if current_user.is_authenticated:
        # app.logger.debug(f'El usuario esta autenticado')
        return redirect(url_for('public.index'))
    form = SignupForm()
    error = None
    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        password = form.password.data
        valid_email = User.get_by_email(email)
        valid_user = User.get_by_name(name)
        if valid_email is not None:
            error = f'El email {valid_email} ya esta siendo utilizado por otro usuario'
        elif valid_user is not None:
            error = f'El nombre de usuario {valid_user} ya esta siendo utilizado por otro usuario'
        else:
            user = User(name=name, email=email)
            user.set_password(password)
            user.save()
            send_email(subject='Bienvenido al miniblog',
                       sender=current_app.config['DONT_REPLY_FROM_EMAIL'],
                       recipients=[email, ],
                       text_body=f'Hola {name}, bienvenid@ al miniblog de Flask',
                       html_body=f'<p>Hola <strong>{name}</strong>, bienvenid@ al miniblog de Flask</p>')
            login_user(user, remember=True)
            next_page = request.args.get('next', None)
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('public.index')
            return redirect(next_page)
    return render_template('auth/signup_form.html', form=form, error=error)


@login_manager.user_loader
def load_user(user_id):
    return User.get_by_id(int(user_id))


@auth_bp.route('/login', methods=['GET', 'POST'])
def login():
    # app.logger.debug(current_user)
    if current_user.is_authenticated:
        # app.logger.debug(f'El usuario esta autenticado')
        return redirect(url_for('public.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.get_by_email(form.email.data)
        if user is not None and user.check_password(form.password.data):
            login_user(user, remember=form.remember_me.data)
            next_page = request.args.get('next')
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('public.index')
            return redirect(next_page)
    return render_template('auth/login_form.html', form=form)


@auth_bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('public.index'))
