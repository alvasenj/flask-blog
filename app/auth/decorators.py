from functools import wraps
from flask import abort, current_app
from flask_login import current_user


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kws):
        is_admin = getattr(current_user, 'is_admin', False)
        if not is_admin:
            current_app.logger.info(f'Intento de acceso no autorizado por usuario: {getattr(current_user, "name", False)}')
            abort(401)
        return f(*args, **kws)
    return decorated_function
