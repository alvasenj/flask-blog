from . import BaseTestClass
from ..auth.models import User
from ..models import Post


class BlogClientTestCase(BaseTestClass):

    def test_index_with_no_posts(self):
        res = self.client.get('/')
        self.assertEqual(200, res.status_code)
        self.assertIn(b'No hay entradas', res.data)

    def test_index_with_posts(self):
        with self.app.app_context():
            user = User.get_by_email('admin@xyz.com')
            post = Post(user_id=user.id, title='Post de prueba', content='Lorem Ipsum')
            post.save()
            res = self.client.get('/')
            self.assertIsNot(b'No hay entradas', res.data)

    def test_redirect_to_login(self):
        res = self.client.get('/admin/')
        self.assertEqual(302, res.status_code)
        self.assertIn('login', res.location)

    def test_unauthorized_access_to_admin(self):
        self.login('guest@xyz.com', '1111')
        res = self.client.get('/admin/')
        self.assertEqual(401, res.status_code)
        self.assertIn(b'Ooops!! No tienes permisos de acceso', res.data)

    def test_authorized_access_to_admin(self):
        self.login('admin@xyz.com', '1111')
        res = self.client.get('/admin/')
        self.assertEqual(200, res.status_code)
        self.assertIn(b'Usuarios', res.data)

    def test_page_not_found(self):
        res = self.client.get('/prueba/')
        self.assertEqual(404, res.status_code)
        self.assertIn(b'Ooops!! La pagina que buscas no existe xD', res.data)

    def test_admin_list_users(self):
        self.login('admin@xyz.com', '1111')
        res = self.client.get('/admin/users')
        self.assertEqual(200, res.status_code)
        self.assertIsNot(b'No hay usuarios', res.data)

    def test_admin_with_no_posts(self):
        self.login('admin@xyz.com', '1111')
        res = self.client.get('/admin/posts')
        self.assertEqual(200, res.status_code)
        self.assertIn(b'No hay entradas', res.data)
        self.app.logger.debug(f'Respuesta: {res.data}')

    def test_admin_with_posts(self):
        with self.app.app_context():
            user = User.get_by_email('admin@xyz.com')
            post = Post(user_id=user.id, title='Post de prueba', content='Lorem Ipsum')
            post.save()
            self.login('admin@xyz.com', '1111')
            res = self.client.get('/admin/posts')
            self.assertEqual(200, res.status_code)
            self.assertIsNot(b'No hay entradas', res.data)

