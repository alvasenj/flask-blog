from app.auth.models import User
from app.models import Post, Comment
from app.tests import BaseTestClass


class CommentModelTestCase(BaseTestClass):

    def test_comment_with_no_login(self):
        with self.app.app_context():
            user = User.get_by_email('admin@xyz.com')
            post = Post(user_id=user.id, title='Post de prueba', content='Lorem Ipsum')
            post.save()
            res = self.client.get(f'/p/{post.title_slug}/')
            self.assertEqual(200, res.status_code)
            self.assertIsNot(b'value="Comentar', res.data)

    def test_comment_with_login(self):
        with self.app.app_context():
            self.login('admin@xyz.com', '1111')
            user = User.get_by_email('admin@xyz.com')
            post = Post(user_id=user.id, title='Post de prueba', content='Lorem Ipsum')
            post.save()
            comment = Comment(content='Lorem Ipsum', user_id=user.id, user_name=user.name, post_id=post.id)
            comment.save()
            res = self.client.get(f'/p/{post.title_slug}/')
            self.assertEqual(200, res.status_code)
            self.assertIn(b'El usuario admin ha comentado', res.data)
