from app.auth.models import User
from . import BaseTestClass


class UserModelTestCase(BaseTestClass):

    def test_admin_delete_user(self):
        with self.app.app_context():
            self.login('admin@xyz.com', '1111')
            user = User.get_by_email('guest@xyz.com')
            user_id = user.id
            res = self.client.get(f'/admin/user/delete/{user_id}')
            self.assertEqual(302, res.status_code)
            self.assertIn('admin/users', res.location)

    def test_update_user(self):
        with self.app.app_context():
            self.login('admin@xyz.com', '1111')
            user = User.get_by_email('guest@xyz.com')
            user.is_admin = True
            user.save()
            self.login('guest@xyz.com', '1111')
            res = self.client.get('/admin/')
            self.assertEqual(200, res.status_code)
