import secrets
from os.path import abspath, dirname, join

# Define el directorio de la aplicacion
BASE_DIR = dirname(dirname(abspath(__file__)))

MEDIA_DIR = join(BASE_DIR, 'media')
POSTS_IMAGES_DIR = join(MEDIA_DIR, 'posts')

SECRET_KEY = secrets.token_hex(20)

# Configuracion de la bd
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Variables de entorno
APP_ENV_LOCAL = 'local'
APP_ENV_TESTING = 'testing'
APP_ENV_DEVELOPMENT = 'development'
APP_ENV_STAGING = 'staging'
APP_ENV_PRODUCTION = 'production'
APP_ENV = ''

LOGS_LOCATION = f'{BASE_DIR}/logs/'
LOG_FILE = f'{LOGS_LOCATION}/app_log.log'

ITEMS_PER_PAGE = 3